import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    items = [
          {
            label:'Home', icon:'pi pi-fw pi-home', routerLink:['/home']},
          {
            label:'Form Components', icon:'pi pi-fw pi-file',
                items:[
                      {
                        label:'Formularios',
                        icon:'pi pi-fw pi-plus',
                        items:[
                        {
                            label:'Autocomplete',
                            icon:'pi pi-fw pi-file',
                            routerLink:['/modulospages/home/autocomplete']
                        },
                        {
                            label:'Checkbox',
                            icon:'pi pi-fw pi-file',
                            routerLink:['/modulospages/home/checkbox']
                        },
                        {
                          label:'Dropdown',
                          icon:'pi pi-fw pi-file',
                          routerLink:['/modulospages/home/dropdown']
                        },
                        {
                          label:'Radiobutton',
                          icon:'pi pi-fw pi-file',
                          routerLink:['/modulospages/home/radiobutton']
                        },
                      ]     
                      },
                    
                ]
          },
            {
              label:'DATA', icon:'pi pi-fw pi-database',
                items:[
                  {
                    label:'Data',
                    icon:'pi pi-fw pi-plus',
                    items:[
                      {
                        label:'Data View',
                        icon:'pi pi-fw pi-file',
                        routerLink:['/modulospages/home/dataview']
                    },
                    {
                        label:'Table',
                        icon:'pi pi-fw pi-table',
                        routerLink:['/modulospages/home/table']
                    },
                    {
                      label:'Table-Striped',
                      icon:'pi pi-fw pi-table',
                      routerLink:['/modulospages/home/striped']
                    },
                    ]  
                  }
                ]  
                
            },
          
    
    
    
    
    ]

  constructor() { }

  ngOnInit(): void { }

}
