import { Component, OnInit } from '@angular/core';
import { ClubesFutbol } from 'src/app/model/clubesFutbol.interface';
import { ClubesService } from '../service/clubes.service';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  clubes:ClubesFutbol[];
  selectedClub: "";
  selectedClubCountry: ClubesFutbol;

  constructor(private clubesfutbol:ClubesService) { 
    
  }

  ngOnInit(): void {
    this.getClubes()   
  }

  

  getClubes(){
    this.clubesfutbol.getClubes().subscribe({
      next:(res)=>{
        console.log(res)
        this.clubes=res
      },
      error:()=>{
        alert(`Cannot Get`);
      }
    })
  
  }
 

}
