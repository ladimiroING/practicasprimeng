import { Component, OnInit } from '@angular/core';

import { Producto } from 'src/app/model/producto.interface';
import { ProductoService } from '../service/producto.service';

@Component({
  selector: 'app-dataview',
  templateUrl: './dataview.component.html',
  styleUrls: ['./dataview.component.scss']
})
export class DataviewComponent implements OnInit {

  products:Producto[];

  
  constructor(private productoSvc:ProductoService) { }

  ngOnInit(): void {
      
    
    this.productoSvc.getProductos().subscribe({
      next:(res)=>{
        console.log(res)
        this.products=res
      },
      error:()=>{
        alert(`Cannot Get`);
      }
    })

    
  }

}
