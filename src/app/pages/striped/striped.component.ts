import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model';
import { UsuarioService } from '../service/usuario.service';

@Component({
  selector: 'app-striped',
  templateUrl: './striped.component.html',
  styleUrls: ['./striped.component.css']
})
export class StripedComponent implements OnInit {

  usuarios:Usuario[]
  
  constructor(private usuarioSvc:UsuarioService) { }

  ngOnInit(): void {
    this.usuarioSvc.getUsuarios().subscribe(usuarios=>{
      console.log(usuarios);
      this.usuarios=usuarios;
    })
  }

}
