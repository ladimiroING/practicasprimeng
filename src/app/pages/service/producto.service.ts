import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Producto } from "src/app/model/producto.interface";



@Injectable({
    providedIn:'root'
})
export class ProductoService{
    
    private apiURL='http://localhost:3000/productos'

    constructor(private http:HttpClient){}
    
    getProductos():Observable<Producto[]>{
        return this.http.get<Producto[]>(this.apiURL)
    }
}