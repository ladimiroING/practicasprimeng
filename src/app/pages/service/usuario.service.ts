import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { map, Observable } from "rxjs"
import { ReqResponseUser, Usuario } from "src/app/model"


@Injectable({
    providedIn:'root'
})
export class UsuarioService{
    
    private apiURL='https://reqres.in/api/users?page=2'

    constructor(private http:HttpClient){}
    
    getUsuarios():Observable<any>{
        return this.http.get<ReqResponseUser>(this.apiURL)
            .pipe(
                map(res=>{
                    return res.data.map(user=>Usuario.usuarioJSON(user))
                })
            )
    
    }
      
}
