import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Pais } from "src/app/model/pais.interface";
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})
export class PaisService {
    
    private apiURL='http://localhost:3000/paises'
    
    constructor(private http:HttpClient) { }


    getPaises():Observable<Pais[]>{
      return this.http.get<Pais[]>(this.apiURL)
    }

    
        
    
}
