import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { City } from "src/app/model/city.interface"


@Injectable({
    providedIn:'root'
})
export class CiudadService {
    
    private apiURL='http://localhost:3000/ciudades'
    
    constructor(private http:HttpClient) { }


    getCiudades():Observable<City[]>{
      return this.http.get<City[]>(this.apiURL)
    }

}
