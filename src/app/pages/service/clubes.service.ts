import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { ClubesFutbol } from "src/app/model/clubesFutbol.interface"


@Injectable({
    providedIn:'root'
})
export class ClubesService {
    
    private apiURL=' http://localhost:3000/clubes'
    
    constructor(private http:HttpClient) { }


    getClubes():Observable<ClubesFutbol[]>{
      return this.http.get<ClubesFutbol[]>(this.apiURL)
    }

    
        
    
}
