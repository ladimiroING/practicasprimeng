import { Component, OnInit } from '@angular/core';
import { City } from 'src/app/model/city.interface';
import { CiudadService } from '../service/ciudad.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  citys:City[]

  constructor(private citySvc:CiudadService) { }

  ngOnInit(): void {
    this.getCiudades()
  }

  getCiudades(){
    this.citySvc.getCiudades().subscribe({
      next:(res)=>{
        console.log(res)
        this.citys=res
      },
      error:()=>{
        alert(`Cannot Get`);
      }
    })
  
  
  }

}
