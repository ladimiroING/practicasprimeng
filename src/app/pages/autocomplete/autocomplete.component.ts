import { Component, OnInit } from '@angular/core';

import { Pais } from 'src/app/model/pais.interface';
import { PaisService } from '../service/pais.service';




@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {

  
  selectedCountry: any;
  filteredCountries: any[];
  countries: any[];

  seleccionPaises: any[];
  filtradoPaises: any[];
  


  
  constructor(private paisSvc:PaisService) { }

  ngOnInit(): void {
    this.getPaises();                       
  }


  //Demo-1
  
  filterCountry(event) {
    
    let filtered : any[] = [];
    let query = event.query;
    
    for(let i = 0; i < this.countries.length; i++) {
      let country = this.countries[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    
    this.filteredCountries = filtered;
  }
  
  getPaises(){
    this.paisSvc.getPaises().subscribe({
      next:(res)=>{
        console.log(res)
        this.countries=res
      },
      error:()=>{
        alert(`Cannot Get`);
      }
    })
  }
  
  search(){
    console.log(this.selectedCountry)
  }


  //Demo-2

  filtroPaises(event) {
    
    let filtered : any[] = [];
    let query = event.query;
    
    for(let i = 0; i < this.countries.length; i++) {
      let country = this.countries[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    
    this.filtradoPaises = filtered;
  }
  
  
   search2(){
    console.log(this.seleccionPaises)
  }




}


  


