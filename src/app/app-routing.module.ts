import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  {
    path:'modulospages',
    loadChildren:()=>import ('./modulospages/modulospages.module').then(m=>m.ModulospagesModule)
  },
  {
    path:'**',
    redirectTo:'modulospages'
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
