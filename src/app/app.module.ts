
//Modulos por defecto de la app
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


//Componente por defecto
import { AppComponent } from './app.component';

//Importacion de modulos
import { AppRoutingModule } from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';






@NgModule({
  declarations: [
    AppComponent,
  ],
  
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
