import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../header/home/home.component";
import { AutocompleteComponent } from "../pages/autocomplete/autocomplete.component";
import { CheckboxComponent } from "../pages/checkbox/checkbox.component";
import { DataviewComponent } from "../pages/dataview/dataview.component";
import { DropdownComponent } from "../pages/dropdown/dropdown.component";
import { RadiobuttonComponent } from "../pages/radiobutton/radiobutton.component";
import { StripedComponent } from "../pages/striped/striped.component";
import { TableComponent } from "../pages/table/table.component";

const rutas: Routes=[
    
    {
        path:'',
        children:[
            {path:'home', component:HomeComponent,
            children:[
                {path:'autocomplete', component:AutocompleteComponent},
                {path:'checkbox', component:CheckboxComponent},
                {path:'dataview', component:DataviewComponent},
                {path:'dropdown', component:DropdownComponent},
                {path:'radiobutton', component:RadiobuttonComponent},
                {path:'table', component:TableComponent},
                {path:'striped', component:StripedComponent},
            ]
            },
            {path:'**', redirectTo:'home'}
        ]
    }
]

@NgModule({
    declarations:[
        
    ],
    imports:[
        RouterModule.forChild(rutas),
        
    ]
})
export class RutasPagesModule{}