import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//Modulos PrimeNG
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ButtonModule} from 'primeng/button';
import {CheckboxModule} from 'primeng/checkbox';
import {DataViewModule} from 'primeng/dataview';
import {DropdownModule} from 'primeng/dropdown';
import { MenubarModule } from 'primeng/menubar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {OrderListModule} from 'primeng/orderlist';
import {TableModule} from 'primeng/table';

//Componentes de la aplicacion
import { AutocompleteComponent } from '../pages/autocomplete/autocomplete.component';
import { CheckboxComponent } from '../pages/checkbox/checkbox.component';
import { DataviewComponent } from '../pages/dataview/dataview.component';
import { DropdownComponent } from '../pages/dropdown/dropdown.component';
import { HomeComponent } from '../header/home/home.component';
import { NavbarComponent } from '../header/navbar/navbar.component';
import { RadiobuttonComponent } from '../pages/radiobutton/radiobutton.component';



import { RutasPagesModule } from './rutaspages.module';
import { ProductoService } from '../pages/service/producto.service';
import { PaisService } from '../pages/service/pais.service';
import { TableComponent } from '../pages/table/table.component';
import { CiudadService } from '../pages/service/ciudad.service';
import { ClubesService } from '../pages/service/clubes.service';
import {UsuarioService} from '../pages/service/usuario.service'
import { HttpClientModule } from '@angular/common/http';
import { StripedComponent } from '../pages/striped/striped.component';






@NgModule({
  declarations: [
    AutocompleteComponent,
    CheckboxComponent,
    DataviewComponent,
    DropdownComponent,
    HomeComponent,
    NavbarComponent,
    RadiobuttonComponent,
    TableComponent,
    StripedComponent

  ],
  exports:[
    AutocompleteComponent,
    CheckboxComponent,
    DataviewComponent,
    DropdownComponent,
    HomeComponent,
    NavbarComponent,
    RadiobuttonComponent,
    TableComponent,
    StripedComponent
  ],
  imports: [
    AutoCompleteModule,
    ButtonModule,
    CheckboxModule,
    DataViewModule,
    DropdownModule,
    FormsModule,
    MenubarModule,
    RadioButtonModule,
    RutasPagesModule,
    OrderListModule,
    TableModule,
    HttpClientModule
  ],
  providers:[
    ProductoService,
    PaisService,
    CiudadService,
    ClubesService,
    UsuarioService
    
  ]
})
export class ModulospagesModule { }
