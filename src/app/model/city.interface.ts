export interface City {
    country:string,
    cityName:string,
    language:string
    totalPopulation:number,
    altitude:number
}